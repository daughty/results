Used AWS free tier EC2 Instances for creating 1 master and 2 worker nodes and using Ansible, created kubernetes cluster.

1. k8install.yml >> For installing docker, kubelet, kubectl and kubeadm
https://www.dropbox.com/s/ndshte5cg2p95p3/Screenshot%202021-04-26%20at%2010.11.02%20AM.png?dl=0

2. master.yml >> For cluster initialization in master node.
https://www.dropbox.com/s/yytdn0ts5hjejnp/Screenshot%202021-04-26%20at%2010.37.17%20AM.png?dl=0

3. worker.yml >> For executing the join command in worker nodes.
https://www.dropbox.com/s/5f4gl61r8h2hr6m/Screenshot%202021-04-26%20at%2010.57.43%20AM.png?dl=0

4. Then copied the ~/.kube/config file to the local machine to access cluster.
https://www.dropbox.com/s/t4pnbohi7pz9xvx/Screenshot%202021-04-28%20at%203.30.12%20PM.png?dl=0

5. In GitLab repo created simple index.html and created a Dockerfile and .gitlab-ci.yml file.

6. Using GitLab CI/CD Pipilines docker image is build.
https://www.dropbox.com/s/4jllzss43bect5q/Screenshot%202021-04-28%20at%203.41.54%20PM.png?dl=0
https://www.dropbox.com/s/xt38qnarhgoero9/Screenshot%202021-04-28%20at%203.43.02%20PM.png?dl=0

7. Logged into docker in the server to pull the image.

8. Created a secret based on the dockerconfigjson.

9. Used the secret in the hello-world-deploy.yml file to pull the image while doing deployment.

10. To make the request roundrobin fashion, edited the configmap in kubeproxy and made the mode "ipvs" with default as "rr".

11. To autoscale the instances according to CPU, used HorizontalPodAutoscaler.

12. Installed nginx-ingress controller using helm and added the secret of tls certificate in hello-world_ingress.yml file.
https://www.dropbox.com/s/xs2uhbokqqfhn88/Screenshot%202021-04-28%20at%204.37.02%20PM.png?dl=0


